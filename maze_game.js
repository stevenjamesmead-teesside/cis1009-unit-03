"use strict";

//The following is needed to get input from the command line.
//At this stage, it is not necessary to understand what it does,
//just accept that it does it.
const input = (function(){
  const i = require('readline-sync');
  return {
    prompt: i.question,
    getkey: i.keyIn
  };
})();

